import { v4 as uuid } from 'uuid'

function generateUUID(prefix = 'p-') {
  const count = Number(process.argv[2]) || 1

  for (let i = 0; i < count; i++) {
    console.log(prefix + uuid())
  }
}

generateUUID()
