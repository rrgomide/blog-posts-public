---
title: Avoid Screen Flickering
date: 2022-12-07
description: A simple trick to avoid screen flickering.
categories:
  - Development
keywords:
  - CSS
---

## TLDR;

- Place the following CSS on the body tag:

```css
body {
  overflow-y: scroll;
}
```

---

## Why?

- When your app needs vertical scroll bars, it will shift your content a little to
  the left, causing a layout shift. It really bothers me.
  Now, if you assume the vertical scroll bar will be always there, the flickering
  won't happen!
- Will the fixed scroll bar annoy users? I don't think so, because browsers usually
  apply some transparency when it is not being effectively used.

---

## Example

- [Check out this repo on repl.it](https://replit.com/@rrgomide/layout-shift-example?v=1)
