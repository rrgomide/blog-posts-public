---
title: The Stack used in this web app
date: 2022-12-06
description: Details on the stack used by this web app.
categories:
  - Development
keywords:
  - JavaScript
  - TypeScript
  - React
  - Remix
---

## The Stack:

- Remix as the Web Framework
- Tailwind CSS for styling
- Netlify for deployment
- PNPM as the dependency manager

---

## Why?

- Regarding Remix, I do want to learn Remix to the fullest. It has a great Developer Experience and if I could, I would bet on it as one the most used Web Frameworks in the near future.
- Regarding Tailwind, I feel really productive with it and it doesn't add that extra processing on the rendering cycle, considering it uses CSS classes.
- Regarding Netlify, I feel really comfortable with its structure and CLI tools. I have other private projects hosted there and never had much trouble handling them.
- Regarding pnpm, it's my first experience with it. It feels sharp and fast.

---

## How about the posts?

I've studied several ways of storing the posts, like using Sanity.io, for example. Nonetheless, I eventually ended up with this:

- A private repository where I write my markdown files and a config.json file.
- Before building and/or starting the Dev Server on Remix, I run a script that:
  - Reads this config file.
  - Fetches all active posts.
  - Generates a static JSON file on the Netlify app repo.
- By doing this I think I save bandwidth fetching data from the repository itself, saving some resources.

---

## Code example

This is the function I run to get all posts and save as a JSON file before Netlify takes place. I've changed some values and variables for the sake of security:

```js
// cache-my-posts.js

const { promises: fs } = require('fs')
const { default: fetch, Headers } = require('node-fetch')
require('dotenv').config()

async function getRawFile(
  slug,
  fullPath,
  accessToken = process.env.TOKEN || '',
  projectID = process.env.PROJECT_ID || '',
  type = 'json'
) {
  /**
   * Fetching file from slug
   */
  console.log(`Fetching ${slug}...`)

  const headers = new Headers()
  headers.set('Access-Token', accessToken)

  const path = encodeURIComponent(fullPath)

  const url =
    `https://repo-url.com/api/v4/projects/${projectID}` +
    `/repository/files/${path}/raw?ref=main`

  const response = await fetch(url, { headers })

  if (!response.ok || response.status !== 200) {
    if (response.status === 404) {
      return undefined
    }
    throw Error(
      `Fetching file from repo failed with ${response.status}: ${response.statusText}`
    )
  }

  if (type === 'json') {
    const json = await response.json()
    return { slug, json }
  }

  /**
   * type ==> text
   */
  const text = await response.text()
  return { slug, text }
}

async function start() {
  console.log('\nExecuting script to fetch all posts...')

  /**
   * Initializing .env variables
   */
  dotEnv.config()

  /**
   * Getting config file
   */
  console.log('Fetching config file...')
  const { json: postsConfig } = await getRawFile(
    'config',
    'posts/_config.json',
    process.env.ACCESS_TOKEN,
    process.env.PROJECT_ID,
    'json'
  )

  /**
   * Filtering active posts Sorting by date
   */
  const activePosts = postsConfig
    .filter(({ active }) => active)
    .sort((a, b) => b.date.localeCompare(a.date))

  const posts = []
  const fetchesToMake = []
  const filesToSave = []

  /**
   * Fetching files
   */
  for (const { slug } of activePosts) {
    fetchesToMake.push(
      getRawFile(
        slug,
        `posts/${slug}.md`,
        process.env.ACCESS_TOKEN,
        process.env.PROJECT_ID,
        'text'
      )
    )
  }

  /**
   * Getting all markdowns
   */
  const markdowns = await Promise.all(fetchesToMake)

  /**
   * Filling posts
   */
  for (const { slug, title, tags, date } of activePosts) {
    const markdown =
      markdowns.find(({ slug: markDownSlug }) => slug === markDownSlug).text ||
      ''

    posts.push({
      slug,
      title,
      tags,
      date,
      markdown,
    })
  }

  /**
   * Saving posts
   */
  filesToSave.push(
    fs.writeFile('path-to/cache.json', JSON.stringify(posts, null, 2))
  )

  /**
   * Saving config
   */
  filesToSave.push(
    fs.writeFile(
      'path-to/config.json',
      JSON.stringify(
        { lastUpdate: new Date().toISOString(), posts: activePosts },
        null,
        2
      )
    )
  )

  console.log('Saving files...')
  await Promise.all(filesToSave)
  console.log('End of script.\n')
}

start()
```

---

## Links

- [Using markdown with Remix](https://andre-landgraf.dev/blog/2022-05-29_how-to-integrate-markdown-content-with-syntax-highlighting-in-remix/)
