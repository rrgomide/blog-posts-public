---
title: Make the computer work for you!
date: 2022-12-21
description: Some tips on automating.
categories:
  - Development
keywords:
  - JavaScript
  - TypeScript
---

## Make the computer work for you!

- Every time you see repetition (well, mostly), automate it!
- Loops to the rescue!
- The maintenace cost will be much cheaper, believe me.

---

## Code sample

Bad:

```jsx
// Component.jsx

function Component() {
  return (
    <ul>
      <li>Route 1</li>
      <li>Route 2</li>
      <li>Route 3</li>
      <li>Route 4</li>
      <li>Route 5</li>
      <li>Route 6</li>
      <li>Route 7</li>
      <li>Route 8</li>
      <li>Route 9</li>
    </ul>
  )
}
```

Good:

```jsx
// ImprovedComponent.jsx

const routes = [
  'Route 1',
  'Route 2',
  'Route 3',
  'Route 4',
  'Route 5',
  'Route 6',
  'Route 7',
  'Route 8',
  'Route 9',
]

function ImprovedComponent() {
  return (
    <ul>
      {routes.map(route => {
        return <li key={route}>{route}</li>
      })}
    </ul>
  )
}
```
