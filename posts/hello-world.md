---
title: Hello, world!
date: 2022-12-05
description: My very first post here.
categories:
  - Development
keywords:
  - JavaScript
  - TypeScript
  - React
---

This is my first post!

A little bit of JavaScript:

```js
function sayHello(name) {
  return `Hello, ${name}!`
}
```

A little bit of TypeScript:

```ts
function sayHello(name: string): string {
  return `Hello, ${name}!`
}
```
